/*
 * Fast User Switching Applet: testapp.c
 * 
 * Copyright (C) 2004-2005 James M. Cape <jcape@ignore-your.tv>.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * Test Application for FUSA backend.
 */

#include <config.h>
#include <glib/gi18n.h>

#include <string.h>

#include <gtk/gtk.h>

#include "fusa-utils.h"
#include "fusa-manager.h"
#include "fusa-user-menu-item.h"


/* **************** *
 *  Private Macros  *
 * **************** */

#define RESPONSE_NEW_CONSOLE	10
#define RESPONSE_NEW_XNEST	11


/* ********************** *
 *  Private Enumerations  *
 * ********************** */

/* Users List Columns */
enum
{
  USER_COLUMN,
  USER_ICON_COLUMN,
  USER_UID_COLUMN,
  USER_NAME_COLUMN,
  USER_DISPLAY_NAME_COLUMN,
  USER_HOME_DIR_COLUMN,
  USER_SHELL_COLUMN,
  USER_DISPLAYS_COLUMN,
  N_USER_COLUMNS
};

/* Displays List Columns */
enum
{
  DISPLAY_COLUMN,
  DISPLAY_NAME_COLUMN,
  DISPLAY_USER_COLUMN,
  DISPLAY_CONSOLE_COLUMN,
  DISPLAY_NESTED_COLUMN,
  N_DISPLAY_COLUMNS
};


/* ******************** *
 *  Private Structures  *
 * ******************** */

typedef struct _TestApp
{
  GtkWidget *app;

  FusaManager *manager;
  gulong display_opened_id;
  gulong display_closed_id;
  gulong user_added_id;
  gulong user_removed_id;

  GtkWidget *users_menu;

  GtkTreeModel *users_model;
  GtkTreeModel *displays_model;
}
TestApp;


/* ********************* *
 *  Function Prototypes  *
 * ********************* */

static void user_uid_cell_data_func  (GtkCellLayout   *layout,
				      GtkCellRenderer *cell,
				      GtkTreeModel    *model,
				      GtkTreeIter     *iter,
				      gpointer         user_data);
static void user_notify_cb           (GObject         *user,
				      GParamSpec      *pspec,
				      gpointer         user_data);
static void user_displays_changed_cb (FusaUser        *user,
				      gpointer         user_data);


/* **************** *
 *  User Functions  *
 * **************** */

/* Cell Renderers */
static void
user_uid_cell_data_func (GtkCellLayout   *layout,
			 GtkCellRenderer *cell,
			 GtkTreeModel    *model,
			 GtkTreeIter     *iter,
			 gpointer         data)
{
  uid_t uid;
  gchar *text;

  uid = 0;
  gtk_tree_model_get (model, iter, USER_UID_COLUMN, &uid, -1);
  
  text = g_strdup_printf ("%u", uid);
  g_object_set (cell, "text", text, NULL);
  g_free (text);
}

static void
user_icon_cell_data_func (GtkCellLayout   *layout,
			  GtkCellRenderer *cell,
			  GtkTreeModel    *model,
			  GtkTreeIter     *iter,
			  gpointer         data)
{
  TestApp *app;
  FusaUser *user;
  GdkPixbuf *pixbuf;

  app = data;
  user = NULL;
  gtk_tree_model_get (model, iter, USER_COLUMN, &user, -1);

  if (user)
    {
      pixbuf = fusa_user_render_icon (user, GTK_WIDGET (app->app), 48,
				      fusa_user_get_n_displays (user));
    }
  else
    pixbuf = NULL;

  g_object_set (cell, "pixbuf", pixbuf, NULL);
  
  if (pixbuf)
    g_object_unref (pixbuf);
}

/* FusaUser Callbacks */
static void
user_notify_cb (GObject    *object,
		GParamSpec *pspec,
		gpointer    data)
{
  TestApp *app;
  FusaUser *user;
  GtkTreeIter iter;

  if (!pspec || !pspec->name)
    return;

  app = data;
  user = FUSA_USER (object);

  if (gtk_tree_model_get_iter_first (app->users_model, &iter))
    {
      FusaUser *tmp_user;
    
      do
	{
	  tmp_user = NULL;
	  gtk_tree_model_get (app->users_model, &iter,
			      USER_COLUMN, &tmp_user,
			      -1);
	  if (user == tmp_user)
	    {
	      if (strcmp (pspec->name, "uid") == 0)
		{
		  gtk_list_store_set (GTK_LIST_STORE (app->users_model), &iter,
				      USER_UID_COLUMN, fusa_user_get_uid (user),
				      -1);
		}
	      else if (strcmp (pspec->name, "user-name") == 0)
		{
		  gtk_list_store_set (GTK_LIST_STORE (app->users_model), &iter,
				      USER_NAME_COLUMN, fusa_user_get_user_name (user),
				      -1);
		}
	      else if (strcmp (pspec->name, "display-name") == 0)
		{
		  gtk_list_store_set (GTK_LIST_STORE (app->users_model), &iter,
				      USER_NAME_COLUMN, fusa_user_get_display_name (user),
				      -1);
		}
	      else if (strcmp (pspec->name, "home-directory") == 0)
		{
		  gtk_list_store_set (GTK_LIST_STORE (app->users_model), &iter,
				      USER_HOME_DIR_COLUMN, fusa_user_get_home_directory (user),
				      -1);
		}
	      else if (strcmp (pspec->name, "shell") == 0)
		{
		  gtk_list_store_set (GTK_LIST_STORE (app->users_model), &iter,
				      USER_SHELL_COLUMN, fusa_user_get_shell (user),
				      -1);
		}

	      g_object_unref (tmp_user);
	      break;
	    }

	  g_object_unref (tmp_user);
	}
      while (gtk_tree_model_iter_next (app->users_model, &iter));
    }
}

static void
user_displays_changed_cb (FusaUser *user,
			  gpointer  data)
{
  TestApp *app;
  GSList *list;
  gchar *value;
  GtkTreeIter iter;

  app = data;
  list = fusa_user_get_displays (user);

  if (list)
    {
      GString *displays;

      displays = g_string_new (NULL);
    
      g_string_append (displays, fusa_display_get_name (list->data));
      list = g_slist_delete_link (list, list);
      while (list)
	{
	  g_string_append (displays, ", ");
	  g_string_append (displays, fusa_display_get_name (list->data));
	  list = g_slist_delete_link (list, list);
	}

      value = displays->str;
      g_string_free (displays, FALSE);
    }
  else
    value = g_strdup ("(none)");

  if (gtk_tree_model_get_iter_first (app->users_model, &iter))
    {
      uid_t uid, tmp_uid;
    
      uid = fusa_user_get_uid (user);
    
      do
	{
	  tmp_uid = -1;
	  gtk_tree_model_get (app->users_model, &iter,
			      USER_UID_COLUMN, &tmp_uid,
			      -1);

	  if (uid == tmp_uid)
	    {
	      gtk_list_store_set (GTK_LIST_STORE (app->users_model), &iter,
				  USER_DISPLAYS_COLUMN, value,
				  -1);
	      break;
	    }
	}
      while (gtk_tree_model_iter_next (app->users_model, &iter));
    }

  g_free (value);
}

/* FusaUserManager Callbacks */
static void
user_added_cb (FusaManager *manager,
	       FusaUser    *user,
	       gpointer     data)
{
  TestApp *app;
  GtkListStore *store;
  GtkTreeIter iter;

  app = data;

  store = GTK_LIST_STORE (app->users_model);

  g_signal_connect (user, "notify", G_CALLBACK (user_notify_cb), app);
  g_signal_connect (user, "displays-changed",
		    G_CALLBACK (user_displays_changed_cb), app);

  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter,
		      USER_COLUMN, user,
		      USER_ICON_COLUMN, NULL,
		      USER_UID_COLUMN, fusa_user_get_uid (user),
		      USER_NAME_COLUMN, fusa_user_get_user_name (user),
		      USER_DISPLAY_NAME_COLUMN, fusa_user_get_display_name (user),
		      USER_HOME_DIR_COLUMN, fusa_user_get_home_directory (user),
		      USER_SHELL_COLUMN, fusa_user_get_shell (user),
		      USER_DISPLAYS_COLUMN, NULL,
		      -1);

  user_displays_changed_cb (user, app);
}

static void
user_removed_cb (FusaManager *manager,
	         FusaUser    *user,
		 gpointer     data)
{
  TestApp *app;
  GtkTreeIter iter;

  app = data;

  if (gtk_tree_model_get_iter_first (app->users_model, &iter))
    {
      uid_t uid, tmp_uid;
    
      uid = fusa_user_get_uid (user);
    
      do
	{
	  tmp_uid = -1;
	  gtk_tree_model_get (app->users_model, &iter,
			      USER_UID_COLUMN, &tmp_uid,
			      -1);

	  if (uid == tmp_uid)
	    {
	      g_signal_handlers_disconnect_by_func (user, user_notify_cb,
						    data);
	      g_signal_handlers_disconnect_by_func (user,
						    user_displays_changed_cb,
						    data);
	      gtk_list_store_remove (GTK_LIST_STORE (app->users_model), &iter);
	      break;
	    }
	}
      while (gtk_tree_model_iter_next (app->users_model, &iter));
    }
}


/* ******************* *
 *  Display Functions  *
 * ******************* */

/* Cell Renderers */
static void
display_console_cell_data_func (GtkCellLayout   *layout,
				GtkCellRenderer *cell,
				GtkTreeModel    *model,
				GtkTreeIter     *iter,
				gpointer         data)
{
  gint console;
  gchar *text;

  console = -1;
  gtk_tree_model_get (model, iter, DISPLAY_CONSOLE_COLUMN, &console, -1);

  if (console >= 0)
    text = g_strdup_printf ("%d", console);
  else
    text = g_strdup ("(Xnest)");

  g_object_set (cell, "text", text, NULL);
  g_free (text);
}

static void
display_user_cell_data_func (GtkCellLayout   *layout,
			     GtkCellRenderer *cell,
			     GtkTreeModel    *model,
			     GtkTreeIter     *iter,
			     gpointer         data)
{
  FusaUser *user;

  user = NULL;
  gtk_tree_model_get (model, iter, DISPLAY_USER_COLUMN, &user, -1);
  
  if (user)
    g_object_set (cell, "text", fusa_user_get_display_name (user), NULL);
  else
    g_object_set (cell, "text", "(none)", NULL);
}

/* FusaDisplay Callbacks */
static void
display_notify_user_cb (GObject    *object,
		        GParamSpec *pspec,
		        gpointer    data)
{
  TestApp *app;
  GtkTreeIter iter;

  app = data;

  if (gtk_tree_model_get_iter_first (app->displays_model, &iter))
    {
      FusaDisplay *display, *tmp_display;
  
      display = FUSA_DISPLAY (object);

      do
	{
	  tmp_display = NULL;
	  gtk_tree_model_get (app->displays_model, &iter,
			      DISPLAY_COLUMN, &tmp_display,
			      -1);

	  if (display == tmp_display)
	    {
	      gtk_list_store_set (GTK_LIST_STORE (app->displays_model), &iter,
				  DISPLAY_USER_COLUMN, fusa_display_get_user (display),
				  -1);
	      g_object_unref (tmp_display);
	      break;
	    }

	  g_object_unref (tmp_display);
	}
      while (gtk_tree_model_iter_next (app->displays_model, &iter));
    }
}

/* FusaDisplayManager Callbacks */
static void
display_opened_cb (FusaManager *manager,
		   FusaDisplay *display,
		   gpointer     data)
{
  TestApp *app;
  GtkListStore *store;
  GtkTreeIter iter;

  app = data;
  store = GTK_LIST_STORE (app->displays_model);

  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter,
		      DISPLAY_COLUMN, display,
		      DISPLAY_NAME_COLUMN, fusa_display_get_name (display),
		      DISPLAY_USER_COLUMN, fusa_display_get_user (display),
		      DISPLAY_CONSOLE_COLUMN, fusa_display_get_console (display),
		      DISPLAY_NESTED_COLUMN, fusa_display_get_nested (display),
		      -1);

  g_signal_connect (display, "notify::user",
		    G_CALLBACK (display_notify_user_cb), data);
}

static void
display_closed_cb (FusaManager *manager,
		   FusaDisplay *display,
		   gpointer     data)
{
  TestApp *app;
  GtkTreeIter iter;

  app = data;

  if (gtk_tree_model_get_iter_first (app->displays_model, &iter))
    {
      FusaDisplay *tmp_display;
    
      do
	{
	  tmp_display = NULL;
	  gtk_tree_model_get (app->displays_model, &iter,
			      DISPLAY_COLUMN, &tmp_display,
			      -1);

	  if (display == tmp_display)
	    {
	      g_signal_handlers_disconnect_by_func (display,
						    display_notify_user_cb,
						    data);
	      gtk_list_store_remove (GTK_LIST_STORE (app->displays_model),
				     &iter);
	      g_object_unref (tmp_display);
	      break;
	    }

	  g_object_unref (tmp_display);
	}
      while (gtk_tree_model_iter_next (app->displays_model, &iter));
    }
}
		  

static void
dialog_response_cb (GtkDialog *dialog,
		    gint       response,
		    gpointer   data)
{
  TestApp *app;

  app = data;

  switch (response)
    {
    case GTK_RESPONSE_CANCEL:
      fusa_die_an_ignominious_death ("Death Error", "You told me to die.",
				     "This is where the GError message goes.");
      break;
    case GTK_RESPONSE_CLOSE:
      gtk_main_quit ();
      break;
    case RESPONSE_NEW_CONSOLE:
      fusa_manager_new_console (app->manager,
				gtk_widget_get_screen (GTK_WIDGET (dialog)));
      break;
    case RESPONSE_NEW_XNEST:
      fusa_manager_new_xnest (app->manager,
			      gtk_widget_get_screen (GTK_WIDGET (dialog)));
      break;
    default:
      g_error ("%s: Unknown response.", G_STRFUNC);
      break;
    }
}

int
main (int   argc,
      char *argv[])
{
  TestApp *app;
  GtkWidget *menubar, *menuitem, *frame, *alignment, *scrwin, *tree_view;
  GtkTreeViewColumn *col;
  GtkCellRenderer *cell;
  GtkListStore *store;
  GSList *list;

  gtk_init (&argc, &argv);

  gtk_rc_parse_string ("style \"fast-user-switch-menubar-style\"\n"
		       "{\n"
		       "  GtkMenuBar::shadow-type = none\n"
		       "  GtkMenuBar::internal-padding = 0\n"
		       "}\n"
		       "widget \"*.fast-user-switch-menubar\" style \"fast-user-switch-menubar-style\"");

  app = g_new0 (TestApp, 1);
  app->manager = fusa_manager_ref_default ();

  app->app = gtk_dialog_new_with_buttons ("Fast User-Switching Test", NULL,
					GTK_DIALOG_NO_SEPARATOR,
					_("New _Console"), RESPONSE_NEW_CONSOLE,
					_("New _Xnest"), RESPONSE_NEW_XNEST,
					_("_Die"), GTK_RESPONSE_CANCEL,
					GTK_STOCK_QUIT, GTK_RESPONSE_CLOSE,
					NULL);
  gtk_window_set_default_size (GTK_WINDOW (app->app), 580, 500);
  g_signal_connect (app->app, "response",
		    G_CALLBACK (dialog_response_cb), app);
  g_signal_connect (app->app, "style-set",
		    G_CALLBACK (fusa_dialog_hig_fix), NULL);

  menubar = gtk_menu_bar_new ();
  gtk_widget_set_name (menubar, "fast-user-switch-menubar");
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (app->app)->vbox), menubar, FALSE, FALSE, 0);
  gtk_widget_show (menubar);
  
  menuitem = gtk_menu_item_new_with_label ("Users");
  gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menuitem);
  gtk_widget_show (menuitem);

  app->users_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), app->users_menu);
  gtk_widget_show (app->users_menu);

  frame = gtk_frame_new ("<b>Users</b>");
  gtk_label_set_use_markup (GTK_LABEL (gtk_frame_get_label_widget (GTK_FRAME (frame))), TRUE);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_NONE);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (app->app)->vbox), frame);
  gtk_widget_show (frame);
  
  alignment = gtk_alignment_new (0.0, 0.0, 1.0, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (alignment), 6, 0, 12, 0);
  gtk_container_add (GTK_CONTAINER (frame), alignment);
  gtk_widget_show (alignment);

  scrwin = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin),
				  GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin),
				       GTK_SHADOW_IN);
  gtk_container_add (GTK_CONTAINER (alignment), scrwin);
  gtk_widget_show (scrwin);

  store = gtk_list_store_new (N_USER_COLUMNS,
			      FUSA_TYPE_USER,
			      GDK_TYPE_PIXBUF,
			      G_TYPE_ULONG,
			      G_TYPE_STRING,
			      G_TYPE_STRING,
			      G_TYPE_STRING,
			      G_TYPE_STRING,
			      G_TYPE_STRING);
  app->users_model = GTK_TREE_MODEL (store);

  list = fusa_manager_list_users (app->manager);
  while (list)
    {
      GtkTreeIter iter;

      gtk_list_store_append (store, &iter);
      gtk_list_store_set (store, &iter,
			  USER_COLUMN, list->data,
			  USER_ICON_COLUMN, NULL,
			  USER_UID_COLUMN, fusa_user_get_uid (list->data),
			  USER_NAME_COLUMN, fusa_user_get_user_name (list->data),
			  USER_DISPLAY_NAME_COLUMN, fusa_user_get_display_name (list->data),
			  USER_HOME_DIR_COLUMN, fusa_user_get_home_directory (list->data),
			  USER_SHELL_COLUMN, fusa_user_get_shell (list->data),
			  USER_DISPLAYS_COLUMN, NULL,
			  -1);

      menuitem = fusa_user_menu_item_new (list->data);
      gtk_menu_shell_append (GTK_MENU_SHELL (app->users_menu), menuitem);
      gtk_widget_show (menuitem);

      g_signal_connect (list->data, "notify",
			G_CALLBACK (user_notify_cb), app);
      g_signal_connect (list->data, "displays-changed",
			G_CALLBACK (user_displays_changed_cb), app);
      user_displays_changed_cb (list->data, app);
    
      list = g_slist_delete_link (list, list);
    }
  g_signal_connect (app->manager, "user-added",
		    G_CALLBACK (user_added_cb), app);
  g_signal_connect (app->manager, "user-removed",
		    G_CALLBACK (user_removed_cb), app);

  tree_view = gtk_tree_view_new_with_model (app->users_model);
  g_object_unref (app->users_model);
  gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
  gtk_widget_show (tree_view);

  cell = gtk_cell_renderer_pixbuf_new ();
  col = gtk_tree_view_column_new ();
  gtk_tree_view_column_pack_start (col, cell, FALSE);
  gtk_cell_layout_set_cell_data_func (GTK_CELL_LAYOUT (col), cell,
				      user_icon_cell_data_func, app, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

  cell = gtk_cell_renderer_text_new ();
  col = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (col, "UID");
  gtk_tree_view_column_pack_start (col, cell, FALSE);
  gtk_cell_layout_set_cell_data_func (GTK_CELL_LAYOUT (col), cell,
				      user_uid_cell_data_func, NULL, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

  cell = gtk_cell_renderer_text_new ();
  col = gtk_tree_view_column_new_with_attributes ("User Name", cell,
						  "text", USER_NAME_COLUMN,
						  NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

  cell = gtk_cell_renderer_text_new ();
  col = gtk_tree_view_column_new_with_attributes ("Display Name", cell,
						  "text", USER_DISPLAY_NAME_COLUMN,
						  NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

  cell = gtk_cell_renderer_text_new ();
  col = gtk_tree_view_column_new_with_attributes ("Home Directory", cell,
						  "text", USER_HOME_DIR_COLUMN,
						  NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

  cell = gtk_cell_renderer_text_new ();
  col = gtk_tree_view_column_new_with_attributes ("Login Shell", cell,
						  "text", USER_SHELL_COLUMN,
						  NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

  cell = gtk_cell_renderer_text_new ();
  col = gtk_tree_view_column_new_with_attributes ("Displays", cell,
						  "text", USER_DISPLAYS_COLUMN,
						  NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

  frame = gtk_frame_new ("<b>Displays</b>");
  gtk_label_set_use_markup (GTK_LABEL (gtk_frame_get_label_widget (GTK_FRAME (frame))), TRUE);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_NONE);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (app->app)->vbox), frame);
  gtk_widget_show (frame);
  
  alignment = gtk_alignment_new (0.0, 0.0, 1.0, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (alignment), 6, 0, 12, 0);
  gtk_container_add (GTK_CONTAINER (frame), alignment);
  gtk_widget_show (alignment);

  scrwin = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin),
				  GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin),
				       GTK_SHADOW_IN);
  gtk_container_add (GTK_CONTAINER (alignment), scrwin);
  gtk_widget_show (scrwin);

  store = gtk_list_store_new (N_DISPLAY_COLUMNS,
			      FUSA_TYPE_DISPLAY,
			      G_TYPE_STRING,
			      FUSA_TYPE_USER,
			      G_TYPE_INT,
			      G_TYPE_BOOLEAN);
  app->displays_model = GTK_TREE_MODEL (store);

  list = fusa_manager_list_displays (app->manager);
  while (list)
    {
      GtkTreeIter iter;

      gtk_list_store_append (store, &iter);
      gtk_list_store_set (store, &iter,
			  DISPLAY_COLUMN, list->data,
			  DISPLAY_NAME_COLUMN, fusa_display_get_name (list->data),
			  DISPLAY_USER_COLUMN, fusa_display_get_user (list->data),
			  DISPLAY_CONSOLE_COLUMN, fusa_display_get_console (list->data),
			  DISPLAY_NESTED_COLUMN, fusa_display_get_nested (list->data),
			  -1);

      g_signal_connect (list->data, "notify::user",
			G_CALLBACK (display_notify_user_cb), app);

      list = g_slist_delete_link (list, list);
    }
  g_signal_connect (app->manager, "display-opened",
		    G_CALLBACK (display_opened_cb), app);
  g_signal_connect (app->manager, "display-closed",
		    G_CALLBACK (display_closed_cb), app);

  tree_view = gtk_tree_view_new_with_model (app->displays_model);
  g_object_unref (app->displays_model);
  gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
  gtk_widget_show (tree_view);

  cell = gtk_cell_renderer_text_new ();
  col = gtk_tree_view_column_new_with_attributes ("X Display", cell,
						  "text", DISPLAY_NAME_COLUMN,
						  NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

  cell = gtk_cell_renderer_toggle_new ();
  col = gtk_tree_view_column_new_with_attributes ("Nested", cell,
						  "active", DISPLAY_NESTED_COLUMN,
						  NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

  cell = gtk_cell_renderer_text_new ();
  col = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (col, "Console");
  gtk_tree_view_column_pack_start (col, cell, FALSE);
  gtk_cell_layout_set_cell_data_func (GTK_CELL_LAYOUT (col), cell,
				      display_console_cell_data_func,
				      NULL, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

  cell = gtk_cell_renderer_text_new ();
  col = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (col, "User");
  gtk_tree_view_column_pack_start (col, cell, TRUE);
  gtk_cell_layout_set_cell_data_func (GTK_CELL_LAYOUT (col), cell,
				      display_user_cell_data_func, NULL, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

  gtk_window_present (GTK_WINDOW (app->app));

  gtk_main ();

  g_object_unref (app->manager);
  gtk_widget_destroy (app->app);
  g_free (app);

  return 0;
}
