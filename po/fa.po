# Persian translation of fast-user-switch-applet.
# Copyright (C) 2006 Sharif FarsiWeb, Inc.
# This file is distributed under the same license as the fast-user-switch-applet package.
# Meelad Zakaria <meelad@farsiweb.info>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: fast-user-switch-applet HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-08-06 15:27+0200\n"
"PO-Revision-Date: 2006-09-04 13:02+0330\n"
"Last-Translator: Roozbeh Pournader <roozbeh@farsiweb.info>\n"
"Language-Team: Persian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../data/GNOME_FastUserSwitchApplet.server.in.in.h:1
msgid "A menu to quickly switch between users"
msgstr "منویی برای تعویض سریع کاربران"

#: ../data/GNOME_FastUserSwitchApplet.server.in.in.h:2 ../src/applet.c:348
msgid "User Switcher"
msgstr "تعویض‌کنندهٔ کاربر"

#: ../data/GNOME_FastUserSwitchApplet.server.in.in.h:3
msgid "User Switcher Applet Factory"
msgstr "کارخانهٔ برنامک تعویض‌کنندهٔ کاربر"

#: ../data/GNOME_FastUserSwitchApplet.xml.h:1
msgid "Edit Personal _Information"
msgstr "ویرایش ا_طلاعات شخصی"

#: ../data/GNOME_FastUserSwitchApplet.xml.h:2
msgid "_About"
msgstr "_درباره"

#: ../data/GNOME_FastUserSwitchApplet.xml.h:3
msgid "_Edit Users and Groups"
msgstr "_ویرایش کاربران و گروه‌ها"

#: ../data/GNOME_FastUserSwitchApplet.xml.h:4
msgid "_Help"
msgstr "ر_اهنما"

#: ../data/GNOME_FastUserSwitchApplet.xml.h:5
msgid "_Preferences"
msgstr "ترجی_حات"

#: ../data/GNOME_FastUserSwitchApplet.xml.h:6
msgid "_Setup Login Screen"
msgstr "برپا_سازی صفحهٔ ورود به سیستم"

#: ../data/fast-user-switch-applet.schemas.in.h:1
msgid "Display Style"
msgstr "سبک نمایش"

#: ../data/fast-user-switch-applet.schemas.in.h:2
msgid "Lock Screen After Switch"
msgstr "قفل کردن صفحهٔ نمایش پس از تعویض"

#: ../data/fast-user-switch-applet.schemas.in.h:3
msgid "Show \"Login Window\" Menuitem"
msgstr "نشان دادن مورد «پنجرهٔ ورود به سیستم» در منو"

#: ../data/fast-user-switch-applet.schemas.in.h:4
msgid "Show \"Other\" Menuitem"
msgstr "نشان دادن مورد «غیره» در منو"

#: ../data/fast-user-switch-applet.schemas.in.h:5
msgid "Show Active Users Only"
msgstr "نمایش فقط کاربران فعال"

#: ../data/fast-user-switch-applet.schemas.in.h:6
msgid ""
"Specifies how to display the applet in the panel. Use \"username\" to "
"display the current user's name, \"icon\" to show the people icon, or \"text"
"\" to use the word `Users.'"
msgstr ""
"چگونگی نمایش برنامک در تابلو را مشخص می‌کند. برای نمایش نام کاربر فعلی از «username»، "
"برای نمایش شمایل افراد از «icon» و برای نمایش کلمهٔ «کاربران» از «text» استفاده کنید."

#: ../data/fast-user-switch-applet.schemas.in.h:7
msgid "Use Xnest"
msgstr "استفاده از Xnest"

#: ../data/fast-user-switch-applet.schemas.in.h:8
msgid ""
"When to show the \"Login Window\" item. Possible values include: \"always\" "
"to always show the item, \"never\" to never show the item, and \"auto\" (the "
"default) to show the item when the applet is in Xnest mode."
msgstr ""
"زمان نمایش مورد «پنجرهٔ ورود به سیستم» در منو. مقادیر ممکن عبارتند از: «always» برای نمایش "
"همیشگی مورد، «never» برای این که مورد هیچ وقت نمایش داده نشود و «auto» (پیش‌فرض) "
"برای این که مورد مواقعی که برنامک در حالت Xnest است نمایش داده شود."

#: ../data/fast-user-switch-applet.schemas.in.h:9
msgid ""
"When to show the \"Other\" item. Possible values include: \"always\" to "
"always show the item, \"never\" to never show the item, and \"auto\" (the "
"default) to show the item when the applet is in console (not Xnest) mode."
msgstr ""
"زمان نمایش مورد «غیره» در منو. مقادیر ممکن عبارتند از: «always» برای نمایش "
"همیشگی مورد، «never» برای این که مورد هیچ وقت نمایش داده نشود و «auto» (پیش‌فرض) "
"برای این که مورد در مواقعی که برنامک در حالت پیشانه (و نه Xnest) است نمایش داده شود."

#: ../data/fast-user-switch-applet.schemas.in.h:10
msgid ""
"Whether or not to create new Xnest windows instead of spawning new consoles "
"when switching users."
msgstr "این که هنگام تعویض کاربر به جای تولید پیشانهٔ جدید، پنجرهٔ Xnest جدید ایجاد بشود یا نه."

#: ../data/fast-user-switch-applet.schemas.in.h:11
msgid ""
"Whether or not to lock the screen after switching to a different console."
msgstr "این که پس از تعویض به پیشانهٔ دیگر صفحهٔ نمایش قفل بشود یا نه."

#: ../data/fast-user-switch-applet.schemas.in.h:12
msgid "Whether to show only users who are currently logged in, or all users."
msgstr "این که فقط کاربرانی که در حال حاضر داخل سیستم هستند نشان داده شوند یا همهٔ کاربران."

#: ../data/ui.glade.h:1
msgid "<span weight=\"bold\" size=\"larger\">Multiple Logins Found</span>"
msgstr "<span weight=\"bold\" size=\"larger\">چند کاربر داخل سیستم هستند</span>"

#: ../data/ui.glade.h:2
msgid "Appearance"
msgstr "ظاهر"

#: ../data/ui.glade.h:3 ../src/applet.c:1981
msgid "Continue"
msgstr "ادامه"

#: ../data/ui.glade.h:4
msgid "Create new logins in _nested windows"
msgstr "ایجاد ورود جدید به سیستم در پنجره‌های _کشویی"

#: ../data/ui.glade.h:5
msgid "Details"
msgstr "جزئیات"

#: ../data/ui.glade.h:6
msgid "Multiple Logins Found - User Switcher"
msgstr "چند کاربر داخل سیستم هستند - تعویض‌کنندهٔ کاربر"

#: ../data/ui.glade.h:7
msgid "Options"
msgstr "گزینه‌ها"

#: ../data/ui.glade.h:8
msgid "Some preferences have been locked by the system adminstrator."
msgstr "بعضی ترجیحات توسط مدیر سیستم قفل شده‌اند."

#: ../data/ui.glade.h:9 ../src/applet.c:2006
msgid ""
"The user you want to switch to is logged in multiple times on this computer. "
"Which login do you want to switch to?"
msgstr "کاربری که انتخاب کردید چند بار در این کامپیوتر وارد سیستم شده است. کدام ورود به سیستم را می‌خواهید؟"

#: ../data/ui.glade.h:10
msgid "Use the `people' icon for the menu title"
msgstr "استفاده از شمایل «افراد» به جای عنوان منو"

#: ../data/ui.glade.h:11
msgid "Use the current user's name for the menu title"
msgstr "استفاده از نام کاربر فعلی به جای عنوان منو"

#: ../data/ui.glade.h:12
msgid "Use the word `Users' as the menu title"
msgstr "استفاده از کلمهٔ «کاربران» به جای عنوان منو"

#: ../data/ui.glade.h:13
msgid "User Switcher Error"
msgstr "خطای تعویض‌کنندهٔ کاربر"

#: ../data/ui.glade.h:14
msgid "User Switcher Preferences"
msgstr "ترجیحات تعویض‌کنندهٔ کاربر"

#: ../data/ui.glade.h:15 ../src/applet.c:471 ../src/applet.c:478
#: ../src/applet.c:1396 ../src/applet.c:1401 ../src/applet.c:1414
#: ../src/applet.c:1419
msgid "Users"
msgstr "کاربران"

#: ../data/ui.glade.h:16
msgid ""
"When a new login must be created to switch users, create it in a window "
"instead of on a new screen"
msgstr "وقتی برای تعویض کاربران لازم است ورود جدیدی به سیستم ایجاد شود، این ورود به جای صفحهٔ نمایش جدید در پنجرهٔ جدید ایجاد شود."

#: ../data/ui.glade.h:17
msgid ""
"When changing to a different display, activate the screensaver for this "
"display."
msgstr "هنگام تعویض به یک نمایش دیگر، محافظ صفحهٔ نمایش این نمایش فعال شود."

#: ../data/ui.glade.h:18
msgid "_Lock the screen after switching users"
msgstr "_قفل کردن صفحهٔ نمایش پس از تعویض کاربران"

#: ../src/applet.c:531
msgid "Other"
msgstr "غیره"

#: ../src/applet.c:544
msgid "Login Window"
msgstr "پنجرهٔ ورود به سیستم"

#: ../src/applet.c:1129
msgid ""
"The Fast User Switch Applet is free software; you can redistribute it and/or "
"modify it under the terms of the GNU General Public License as published by "
"the Free Software Foundation; either version 2 of the License, or (at your "
"option) any later version."
msgstr ""

#: ../src/applet.c:1133
msgid ""
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for "
"more details."
msgstr ""

#: ../src/applet.c:1137
msgid ""
"You should have received a copy of the GNU General Public License along with "
"this program; if not, write to the Free Software Foundation, Inc., 51 "
"Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA "
msgstr ""

#: ../src/applet.c:1158
msgid "A menu to quickly switch between users."
msgstr "منویی برای تعویض سریع بین کاربران."

#: ../src/applet.c:1177
msgid "translator-credits"
msgstr ""
"میلاد زکریا <meelad@farsiweb.info>\n"
"روزبه پورنادر <roozbeh@farsiweb.info>"

#: ../src/applet.c:1975
msgid "Multiple Logins Found - Fast User-Switch"
msgstr "چند کاربر داخل سیستم هستند - تعویض سریع کاربر"

#: ../src/applet.c:2005
msgid "Multiple Logins Found"
msgstr "چند کاربر وارد سیستم هستند"

#: ../src/applet.c:2023
#, c-format
msgid "%s in a nested window on console %d"
msgstr "%s در یک پنجرهٔ کشویی در پیشانهٔ %Id"

#: ../src/applet.c:2027
#, c-format
msgid "%s on console %d"
msgstr "%s در پیشانهٔ %Id"

#: ../src/applet.c:2069
msgid "Display Manager Unavailable"
msgstr "مدیر نمایش قابل دسترسی نیست"

#: ../src/applet.c:2070
msgid ""
"In order for the User Selector to function properly, the GNOME Display "
"Manager must be running, but it is not."
msgstr "برای درست کار کردن انتخاب‌گر کاربر باید مدیر نمایش گنوم در حال اجرا باشد، ولی نیست."

#: ../src/applet.c:2074
msgid "Multiple Consoles Not Supported"
msgstr "پیشانه‌های متعدد پشتیبانی نمی‌شود"

#: ../src/applet.c:2075
msgid "This system does not support multiple graphical login screens."
msgstr "سیستم از صفحه‌های ورود به سیستم متعدد پشتیبانی نمی‌کند."

#: ../src/applet.c:2080
msgid "Too Many Sessions"
msgstr "تعداد نشست‌ها زیاد است"

#: ../src/applet.c:2081
msgid ""
"There are currently too many sessions running to create a new one. Someone "
"must log out, or the system configuration must be changed to allow for more."
msgstr ""
"تعداد نشست‌های در حال اجرا بیش از آن است که بتوان نشست دیگری ایجاد کرد. یا باید کسی "
"از سیستم خارج شود، و یا پیکربندی سیستم باید طوری تغییر کند که بتوان نشست‌های بیشتری داشت."

#: ../src/applet.c:2087
msgid "Graphical System Error"
msgstr "خطای سیستم گرافیکی"

#: ../src/applet.c:2088
msgid ""
"The graphical interface did not properly start, which is needed for "
"graphical logins. It is likely that the X Window System or GDM is not "
"properly configured."
msgstr ""
"واسط گرافیکی که برای ورود گرافیکی به سیستم لازم است درست آغاز نشد. به احتمال زیاد "
"سیستم پنجره‌ای X یا GDM درست پیکربندی نشده‌اند."

#: ../src/applet.c:2093
msgid "Permissions Error"
msgstr "خطای اجازه"

#: ../src/applet.c:2094
msgid ""
"Your user does not have permissions to create additional logins.  Check your "
"<i>~/.Xauthority</i> setup."
msgstr ""
"کاربر شما اجازهٔ ورود متعدد به سیستم را ندارد. برپاسازی <i>~/.Xauthority</i> خود را بررسی کنید."

#: ../src/applet.c:2239
msgid "Missing Required File"
msgstr "یک پروندهٔ ضروری مفقود است"

#: ../src/applet.c:2241
#, c-format
msgid ""
"The User Selector's interfaces file, `%s', could not be opened. It is likely "
"that this application was not properly installed or configured."
msgstr "پروندهٔ واسط انتخاب‌گر کاربر، «%s» را نمی‌توان باز کرد. به احتمال زیاد برنامه درست نصب یا پیکربندی نشده است."

#: ../src/applet.c:2317
#, c-format
msgid "Can't lock screen: %s"
msgstr "نمی‌توان صفحهٔ نمایش را قفل کرد: %s"

#: ../src/applet.c:2331
#, c-format
msgid "Can't temporarily set screensaver to blank screen: %s"
msgstr "نمی‌توان محافظ صفحهٔ نمایش را موقتاً به صفحهٔ خالی تغییر داد: %s"

#: ../src/fusa-display.c:119 ../src/fusa-user.c:150
msgid "Manager"
msgstr "مدیر"

#: ../src/fusa-display.c:120
msgid "The manager which owns this object."
msgstr "مدیری که مالک این شیء است."

#: ../src/fusa-display.c:127
msgid "Name"
msgstr "نام"

#: ../src/fusa-display.c:128
msgid "The name of the X11 display this object refers to."
msgstr "نام نمایش x11 که این شیء به آن ارجاع می‌دهد."

#: ../src/fusa-display.c:134 ../src/fusa-user-menu-item.c:163
msgid "User"
msgstr "کاربر"

#: ../src/fusa-display.c:135
msgid "The user currently logged in on this virtual terminal."
msgstr "کاربری که در حال حاضر در این پایانهٔ مجازی وارد سیستم شده است."

#: ../src/fusa-display.c:141
msgid "Console"
msgstr "پیشانه"

#: ../src/fusa-display.c:142
msgid "The number of the virtual console this display can be found on, or %-1."
msgstr "شمارهٔ پیشانهٔ مجازی که این نمایش در آن پیدا می‌شود، یا %-1."

#: ../src/fusa-display.c:148
msgid "Nested"
msgstr "کشویی"

#: ../src/fusa-display.c:149
msgid "Whether or not this display is a windowed (Xnest) display."
msgstr "این که این نمایش پنجره‌دار (Xnest) هست یا نه."

#: ../src/fusa-manager.c:1452
msgid "The display manager could not be contacted for unknown reasons."
msgstr "به دلیل نامعلومی نمی‌توان با مدیر نمایش ارتباط برقرار کرد."

#: ../src/fusa-manager.c:1459
msgid "The display manager is not running or too old."
msgstr "مدیر نمایش در حال اجرا نیست و یا خیلی قدیمی است."

#: ../src/fusa-manager.c:1462
msgid "The configured limit of flexible servers has been reached."
msgstr "محدودهٔ پیکربندی‌شدهٔ کارگزارهای انعطاف‌پذیر پر شده است."

#: ../src/fusa-manager.c:1465
msgid "There was an unknown error starting X."
msgstr "هنگام آغاز X خطای نامعلومی پیش آمد."

#: ../src/fusa-manager.c:1468
msgid "The X server failed to finish starting."
msgstr "کارگزار X نتوانست راه‌اندازی را به پایان برساند."

#: ../src/fusa-manager.c:1471
msgid "There are too many X sessions running."
msgstr "تعداد نشست‌های X در حال اجرا زیاد است."

#: ../src/fusa-manager.c:1474
msgid "The nested X server (Xnest) cannot connect to your current X server."
msgstr "کارگزار X کشویی (Xnest) نمی‌تواند با کارگزار X فعلی شما ارتباط برقرار کند."

#: ../src/fusa-manager.c:1477
msgid "The X server in the GDM configuration could not be found."
msgstr "کارگزار X موجود در پیکربندی GDM پیدا نمی‌شود."

#: ../src/fusa-manager.c:1480 ../src/gdmcomm.c:492
msgid ""
"Trying to set an unknown logout action, or trying to set a logout action "
"which is not available."
msgstr "در حال تلاش برای تنظیم یک کنش خروج از سیستم ناشناخته یا ناموجود."

#: ../src/fusa-manager.c:1483 ../src/gdmcomm.c:495
msgid "Virtual terminals not supported."
msgstr "پایانهٔ مجازی پشتیبانی نمی‌شود."

#: ../src/fusa-manager.c:1486
msgid "Invalid virtual terminal number."
msgstr "شمارهٔ پایانهٔ مجازی نامعتبر."

#: ../src/fusa-manager.c:1489 ../src/gdmcomm.c:499
msgid "Trying to update an unsupported configuration key."
msgstr "در حال تلاش برای به‌هنگام‌سازی یک کلید پیکربندی پشتیبانی نشده."

#: ../src/fusa-manager.c:1492
msgid "~/.Xauthority file badly configured or missing."
msgstr "پروندهٔ ‎~/.Xauthority بد پیکربندی شده یا موجود نیست."

#: ../src/fusa-manager.c:1495
msgid "Too many messages were sent to the display manager, and it hung up."
msgstr "پیغام‌های زیادی به مدیر نمایش فرستاده شد و مدیر نمایش ارتباط را قطع کرد."

#: ../src/fusa-manager.c:1499
msgid "The display manager sent an unknown error message."
msgstr "مدیر نمایش پیغام خطای ناشناخته‌ای فرستاد."

#: ../src/fusa-user-menu-item.c:164
msgid "The user this menu item represents."
msgstr "کاربری که این مورد منو نمایندهٔ اوست."

#: ../src/fusa-user-menu-item.c:171
msgid "Icon Size"
msgstr "اندازهٔ شمایل"

#: ../src/fusa-user-menu-item.c:172
msgid "The size of the icon to use."
msgstr "اندازهٔ شمایل مورد استفاده."

#: ../src/fusa-user-menu-item.c:178
msgid "Indicator Size"
msgstr "اندازهٔ شاخص"

#: ../src/fusa-user-menu-item.c:179
msgid "Size of check indicator"
msgstr "اندازهٔ شاخص شطرنجی"

#: ../src/fusa-user-menu-item.c:184
msgid "Indicator Spacing"
msgstr "فاصله‌گذاری شاخص"

#: ../src/fusa-user-menu-item.c:185
msgid "Space between the username and the indicator"
msgstr "فاصلهٔ بین نام کاربری و شاخص"

#: ../src/fusa-user.c:151
msgid "The user manager object this user is controlled by."
msgstr "شیء مدیر کاربری که این کاربر توسط آن کنترل می‌شود."

#: ../src/fusa-utils.c:80
msgid "Show Details"
msgstr "نمایش جزئیات"

#. markup
#: ../src/gdmcomm.c:413
msgid "GDM (The GNOME Display Manager) is not running."
msgstr "‏‎GDM (مدیر نمایش گنوم) در حال اجرا نیست."

#: ../src/gdmcomm.c:416
msgid ""
"You might in fact be using a different display manager, such as KDM (KDE "
"Display Manager) or xdm."
msgstr "شاید در واقع از یک مدیر نمایش دیگر مثلاً KDM (مدیر نمایش KDE) یا xdm استفاده می‌کنید."

#: ../src/gdmcomm.c:419
msgid ""
"If you still wish to use this feature, either start GDM yourself or ask your "
"system administrator to start GDM."
msgstr "اگر هنوز می‌خواهید از این امکانات استفاده کنید، یا خودتان GDM را به کار بیندازید و یا از مدیر سیستمتان بخواهید این کار را برایتان انجام دهد."

#. markup
#: ../src/gdmcomm.c:441
msgid "Cannot communicate with GDM (The GNOME Display Manager)"
msgstr "نمی‌توان با GDM (مدیر نمایش گنوم) ارتباط برقرار کرد"

#: ../src/gdmcomm.c:444
msgid "Perhaps you have an old version of GDM running."
msgstr "شاید نسخهٔ GDM در حال اجرای شما قدیمی باشد."

#: ../src/gdmcomm.c:463 ../src/gdmcomm.c:466
msgid "Cannot communicate with gdm, perhaps you have an old version running."
msgstr "نمی‌توان با gdm ارتباط برقرار کرد، شاید نسخهٔ در حال اجرای شما قدیمی باشد."

#: ../src/gdmcomm.c:469
msgid "The allowed limit of flexible X servers reached."
msgstr "تعداد مجاز کارگزارهای X انعطاف‌پذیر پر شده است."

#: ../src/gdmcomm.c:471
msgid "There were errors trying to start the X server."
msgstr "هنگام تلاش برای آغاز کارگزار X خطاهایی رخ داد."

#: ../src/gdmcomm.c:473
msgid "The X server failed.  Perhaps it is not configured well."
msgstr "کارگزار X شکست خورد. شاید درست پیکربندی نشده باشد."

#: ../src/gdmcomm.c:476
msgid "Too many X sessions running."
msgstr "تعداد نشست‌های X در حال اجرا زیاد است."

#: ../src/gdmcomm.c:478
msgid ""
"The nested X server (Xnest) cannot connect to your current X server.  You "
"may be missing an X authorization file."
msgstr "کارگزار X کشویی (Xnest) نمی‌تواند به کارگزار X فعلی شما متصل شود. شاید یک پروندهٔ تأیید هویت X در سیستم شما وجود ندارد."

#: ../src/gdmcomm.c:483
msgid ""
"The nested X server (Xnest) is not available, or gdm is badly configured.\n"
"Please install the Xnest package in order to use the nested login."
msgstr ""
"کارگزار X کشویی (Xnest) موجود نیست، یا gdm بد پیکربندی شده است.\n"
"لطفاً بستهٔ Xnest را نصب کنید تا بتوانید از ورود به سیستم کشویی استفاده کنید."

#: ../src/gdmcomm.c:488
msgid ""
"The X server is not available, it is likely that gdm is badly configured."
msgstr "کارگزار X موجود نیست، به احتمال زیاد gdm بد پیکربندی شده است."

#: ../src/gdmcomm.c:497
msgid "Trying to change to an invalid virtual terminal number."
msgstr "در حال تلاش برای تغییر شمارهٔ پایانهٔ مجازی نامعتبر."

#: ../src/gdmcomm.c:501
msgid ""
"You do not seem to have authentication needed be for this operation.  "
"Perhaps your .Xauthority file is not set up correctly."
msgstr "به نظر نمی‌رسد شما اجازهٔ انجام این عملیات را داشته باشید. شاید پروندهٔ ‎.Xauthority شما درست برپاسازی نشده باشد."

#: ../src/gdmcomm.c:505
msgid "Too many messages were sent to gdm and it hung upon us."
msgstr "پیغام‌های زیادی برای gdm فرستاده شد و در نتیجه ارتباط را قطع کرد."

#: ../src/gdmcomm.c:508
msgid "Unknown error occurred."
msgstr "خطای نامعلومی رخ داد."
