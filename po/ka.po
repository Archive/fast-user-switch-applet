# translation of fast-user-switch-applet.HEAD.po to Georgian
# Copyright (C) 2006 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Vladimer Sichinava <alinux@siena.linux.it>, 2006.
# Vladimer Sichinava <vlsichinava@gmail.com>, 2006.
# Vladimer Sichinava  ვლადიმერ სიჭინავა <vsichi@gnome.org>, 2008.
# Vladimer Sichinava  ვლადიმერ სიჭინავა <vsichi@gnome.org>, 2008.
# Vladimer Sichinava  ვლადიმერ სიჭინავა <vsichi@gnome.org>, 2008.
# Vladimer Sichinava  ვლადიმერ სიჭინავა <vsichi@gnome.org>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: fast-user-switch-applet.HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-09-27 00:23+0200\n"
"PO-Revision-Date: 2008-09-27 00:37+0200\n"
"Last-Translator: Vladimer Sichinava  ვლადიმერ სიჭინავა <vsichi@gnome.org>\n"
"Language-Team: Georgian <http://mail.gnome.org/mailman/listinfo/gnome-ge-"
"list>\n"
"MIME-Version: 1.0\n"
"Content-Type:  text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"X-Generator: KBabel 1.11.2\n"

#: ../data/GNOME_FastUserSwitchApplet.server.in.in.h:1
msgid "A menu to quickly switch between users"
msgstr "მომხმარებლის სწრაფი შეცვლის მენიუ"

#: ../data/GNOME_FastUserSwitchApplet.server.in.in.h:2 ../src/applet.c:354
msgid "User Switcher"
msgstr "მომხმარებლის გადამრთველი"

#: ../data/GNOME_FastUserSwitchApplet.server.in.in.h:3
msgid "User Switcher Applet Factory"
msgstr "მომხმარებლის შეცვლის აპლეტის ფაბრიკა"

#: ../data/GNOME_FastUserSwitchApplet.xml.h:1
msgid "Edit Personal _Information"
msgstr "_პირადი ინფორმაციიის რედაქტირება"

#: ../data/GNOME_FastUserSwitchApplet.xml.h:2
msgid "_About"
msgstr "პროგრ_ამის შესახებ"

#: ../data/GNOME_FastUserSwitchApplet.xml.h:3
msgid "_Edit Users and Groups"
msgstr "_მომხმარებლების და ჯგუფების რედაქტირება"

#: ../data/GNOME_FastUserSwitchApplet.xml.h:4
msgid "_Help"
msgstr "_დახმარება"

#: ../data/GNOME_FastUserSwitchApplet.xml.h:5
msgid "_Preferences"
msgstr "_პარამეტრები"

#: ../data/GNOME_FastUserSwitchApplet.xml.h:6
msgid "_Setup Login Screen"
msgstr "_რეგისტრაციის სარკმლის პარამეტრები"

#: ../data/fast-user-switch-applet.schemas.in.h:1
msgid "Display Style"
msgstr "სტილის ჩვენება"

#: ../data/fast-user-switch-applet.schemas.in.h:2
msgid "Lock Screen After Switch"
msgstr "გადართვის შემდეგ ეკრანის ბლოკირება"

#: ../data/fast-user-switch-applet.schemas.in.h:3
msgid "Show \"Login Window\" Menuitem"
msgstr "მენიუში ვაჩვენოთ \"რეგისტრაციის სარკმელი\""

#: ../data/fast-user-switch-applet.schemas.in.h:4
msgid "Show \"Other\" Menuitem"
msgstr "მენიუში ვაჩვენოთ \"სხვა\""

#: ../data/fast-user-switch-applet.schemas.in.h:5
msgid "Show Active Users Only"
msgstr "ვაჩვენოთ მხოლოდ აქტიური მომხმარებლები"

#  ვერაფერიც ვერ გავიგე :(
#: ../data/fast-user-switch-applet.schemas.in.h:6
msgid ""
"Specifies how to display the applet in the panel. Use \"username\" to "
"display the current user's name, \"icon\" to show the people icon, or \"text"
"\" to use the word `Users.'"
msgstr ""
"აპლეტის პანელზე ჩვენების სტილი. \"username\" - მომდინარე მომხმარებლის "
"სახელის ჩვენებისთვის, \"icon\" - მომხამრებლის ესკიზის ჩვენება, ან \"text\" - "
"სიტყვა `მომხმარებლები' გამოსაყენებლად."

#: ../data/fast-user-switch-applet.schemas.in.h:7
msgid "Use Xnest"
msgstr "Xnest-ის გამოყენება "

#: ../data/fast-user-switch-applet.schemas.in.h:8
msgid ""
"When to show the \"Login Window\" item. Possible values include: \"always\" "
"to always show the item, \"never\" to never show the item, and \"auto\" (the "
"default) to show the item when the applet is in Xnest mode."
msgstr ""
"როდის იქნას ნაჩვენები მენიუში \"რეგისტრაციის სარკმელი\". \"always\" - "
"ყოველთვის, \"never\" - არასოდეს, და \"auto\" (ნაგულისხმევი) - როცა აპლეტი "
"Xnest-რეჟიმშია."

#: ../data/fast-user-switch-applet.schemas.in.h:9
msgid ""
"When to show the \"Other\" item. Possible values include: \"always\" to "
"always show the item, \"never\" to never show the item, and \"auto\" (the "
"default) to show the item when the applet is in console (not Xnest) mode."
msgstr ""
"როდის იქნას ნაჩვენები მენიუში \"სხვა\". \"always\" - ყოველთვის, \"never\" - "
"არასოდეს, და \"auto\" (ნაგულისხმევი) - როცა აპლეტი კონსოლის (არა Xnest) "
"რეჟიმშია."

#: ../data/fast-user-switch-applet.schemas.in.h:10
msgid ""
"Whether or not to create new Xnest windows instead of spawning new consoles "
"when switching users."
msgstr ""
"მომხმარებლის შეცვლისას გაიხსნას თუ არა ახალი Xnest-სარკმელი ახალი კონსოლების "
"გახსნის ნაცვლად."

#: ../data/fast-user-switch-applet.schemas.in.h:11
msgid ""
"Whether or not to lock the screen after switching to a different console."
msgstr "დაიბლოკოს თუ არა ეკრანი სხვა კონსოლზე გადართვისას."

#: ../data/fast-user-switch-applet.schemas.in.h:12
msgid "Whether to show only users who are currently logged in, or all users."
msgstr "ნაჩვენები იქნას მხოლოდ ამჟამად რეგისტრირებული, თუ ყველა მომხმარებელი."

#: ../data/ui.glade.h:1
msgid "<span weight=\"bold\" size=\"larger\">Multiple Logins Found</span>"
msgstr ""
"<span weight=\"bold\" size=\"larger\">სისტემაში დაფიქსირდა ერთზე მეტი "
"რეგისტრაცია</span> "

#: ../data/ui.glade.h:2
msgid "Appearance"
msgstr "იერსახე"

#: ../data/ui.glade.h:3 ../src/applet.c:2024
msgid "Continue"
msgstr "გაგრძელება"

#: ../data/ui.glade.h:4
msgid "Create new logins in _nested windows"
msgstr "_ახალი რეგისტრაცია ჩადგმულ ფანჯარაში"

#: ../data/ui.glade.h:5
msgid "Details"
msgstr "ცნობები"

#: ../data/ui.glade.h:6
msgid "Multiple Logins Found - User Switcher"
msgstr "დადგენილია მულტი რეგისტრაცია - მომხმარებელთა გადამრთველი"

#: ../data/ui.glade.h:7
msgid "Options"
msgstr "ოფციები"

#: ../data/ui.glade.h:8
msgid "Some preferences have been locked by the system adminstrator."
msgstr "ზოგი ფუნქცია ბლოკირებულია ადმინისტრატორის მიერ."

#: ../data/ui.glade.h:9 ../src/applet.c:2049
msgid ""
"The user you want to switch to is logged in multiple times on this computer. "
"Which login do you want to switch to?"
msgstr ""
"მომხმარებელი, რომელზეც თქვენ გინდათ გადართვა, რამდენიმეჯერაა რეგისტრირებული "
"ამ კომპიუტერზე. რომელ რეგისტრაციას გამოიყენებთ?"

#: ../data/ui.glade.h:10
msgid "Use the `people' icon for the menu title"
msgstr "მენიუს სათაურად გამოვიყენოთ ხატულა `people'"

#: ../data/ui.glade.h:11
msgid "Use the current user's name for the menu title"
msgstr "მენიუს სათაურად გამოვიყენოთ მიმდინარე მომხმარებლის სახელი"

#: ../data/ui.glade.h:12
msgid "Use the word `Users' as the menu title"
msgstr "მენიუს სათაურად გამოვიყენოთ სიტყვა `მომხმარებლები'"

#: ../data/ui.glade.h:13
msgid "User Switcher Error"
msgstr "შეცდომა მომხმარებლის შეცვლისას"

#: ../data/ui.glade.h:14
msgid "User Switcher Preferences"
msgstr "მომხმარებლის შეცვლის პარამეტრები"

#: ../data/ui.glade.h:15 ../src/applet.c:477 ../src/applet.c:484
#: ../src/applet.c:1408 ../src/applet.c:1413 ../src/applet.c:1426
#: ../src/applet.c:1431
msgid "Users"
msgstr "მომხმარებლები"

#: ../data/ui.glade.h:16
msgid ""
"When a new login must be created to switch users, create it in a window "
"instead of on a new screen"
msgstr ""
"როცა მომხმარებლის გადართვისას საჭირო იქნება ახალი რეგისტრაცია, გაიხსნას "
"ახალი სარკმელი ახალი ეკრანის ნაცვლად"

#: ../data/ui.glade.h:17
msgid ""
"When changing to a different display, activate the screensaver for this "
"display."
msgstr "სხვა დისპლეიზე გადასვლისას ამ დისპლეიზე გააქტიურდეს ეკრანდამცავი."

#: ../data/ui.glade.h:18
msgid "_Lock the screen after switching users"
msgstr "_დაიბლოკოს ეკრანი მომხმარებლის შეცვლისას"

#: ../src/applet.c:332
msgid "Fast User Switch Applet"
msgstr "მომხმარებლის გადამრთველი აპლეტი"

#: ../src/applet.c:548
msgid "Other"
msgstr "სხვა"

#: ../src/applet.c:561
msgid "Login Window"
msgstr "რეგისტრაციის სარკმელი"

#: ../src/applet.c:1134
msgid ""
"The Fast User Switch Applet is free software; you can redistribute it and/or "
"modify it under the terms of the GNU General Public License as published by "
"the Free Software Foundation; either version 2 of the License, or (at your "
"option) any later version."
msgstr ""
"მომხმარებლის სწრაფი შეცვლის აპლეტი - ეს არის თავისუფალი პროგრამა. თქვენ "
"შეგიძლიათ მისი გავრცელება ან/და შეცვლა თავისუფალი პროგრამირების ფონდის მიერ "
"გამოქვეყნებული ერთიანი საზოგადოებრივი ლიცენზიის (GNU) 2 ან უფრო გვიანდელი "
"ვერსიის პირობების შესაბამისად."

#: ../src/applet.c:1138
msgid ""
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for "
"more details."
msgstr ""
"პროგრამის გამავრცელებლები იმედოვნებენ, რომ ის სასარგებლო იქნება, მაგრამ ვერ "
"მოგცემენ რაიმე გარანტიას, განცხადებულს ან ნაგულისხმევს, მათ შორის კომერციული "
"სარგებლიანობისა თუ რაიმე კონკრეტული მიზნისათვის გამოსადეგობის ჩათვლით, ან "
"სხვა ნებისმიერი თვალსაზრისით. დაწვრილებითი ინფორმაციისათვის იხილეთ ერთიანი "
"საზოგადოებრივი ლიცენზია (GNU General Public License)."

#: ../src/applet.c:1142
msgid ""
"You should have received a copy of the GNU General Public License along with "
"this program; if not, write to the Free Software Foundation, Inc., 51 "
"Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA "
msgstr ""
"ამ პროგრამასთან ერთად  უნდა მიგეღოთ GNU ლიცენზია;თუ არ მიგიღიათ,მოგვწერეთ "
"თავისუფალი პროგრამების ფონდში. მისამართზე: 51 Franklin Street, Fifth Floor, "
"Boston, MA 02110-1301, USA"

#: ../src/applet.c:1155
msgid "A menu to quickly switch between users."
msgstr "მომხმარებლის სწრაფი შეცვლის მენიუ."

#: ../src/applet.c:1160
msgid "translator-credits"
msgstr "Malkhaz Barkalaya მალხაზ ბარკალაია <malxaz@gmail.com>"

#: ../src/applet.c:2018
msgid "Multiple Logins Found - Fast User-Switch"
msgstr "დაფიქსირდა ერთზე მეტი რეგისტრაცია - მომხმარებლის სწრაფი შეცვლა"

#: ../src/applet.c:2048
msgid "Multiple Logins Found"
msgstr "სისტემაში დაფიქსირდა ერთზე მეტი რეგისტრაცია"

#: ../src/applet.c:2066
#, c-format
msgid "%s in a nested window on console %d"
msgstr "%s ჩადგმულ სარკმელში კონსოლზე %d"

#: ../src/applet.c:2070
#, c-format
msgid "%s on console %d"
msgstr "%s კონსოლზე %d"

#: ../src/applet.c:2112
msgid "Display Manager Unavailable"
msgstr "დისპლეის მმართველი ხელმიუწვდომელია"

#: ../src/applet.c:2113
msgid ""
"In order for the User Selector to function properly, the GNOME Display "
"Manager must be running, but it is not."
msgstr ""
"მომხმარებელთა სელექტორის სწორად მუშაობისათვის საჭიროა GDM - დისპლეის "
"მმართველი, რომელიც არ არის გაშვებული."

#: ../src/applet.c:2117
msgid "Multiple Consoles Not Supported"
msgstr "ერთზე მეტი კონსოლი არ არის მხარდაჭერილი"

#: ../src/applet.c:2118
msgid "This system does not support multiple graphical login screens."
msgstr "მოცემულ სისტემას არ გააჩნია მრავალ რეგისტრირებადი ეკრანი."

#: ../src/applet.c:2123
msgid "Too Many Sessions"
msgstr "სესიების რაოდენობა დასაშვებზე მეტია"

#: ../src/applet.c:2124
msgid ""
"There are currently too many sessions running to create a new one. Someone "
"must log out, or the system configuration must be changed to allow for more."
msgstr ""
"მეტი სესიის გახსნა შეუძლებელია. ან უნდა დაიხუროს რომელიმე მათგანი, ან "
"შეიცვალოს სისტემის კონფიგურაცია დაშვებული რაოდენობის გასაზრდელად."

#: ../src/applet.c:2130
msgid "Graphical System Error"
msgstr "გრაფიკული სისტემა შეცდომა"

#: ../src/applet.c:2131
msgid ""
"The graphical interface did not properly start, which is needed for "
"graphical logins. It is likely that the X Window System or GDM is not "
"properly configured."
msgstr ""
"გრაფიკული რეგისტრაციისათვის საჭირო გრაფიკული ინტერფეისი ვერ გაიშვა.შეიძლება "
"X Window ან GDM არასწორადაა კონფიგურირებული."

#: ../src/applet.c:2136
msgid "Permissions Error"
msgstr "უფლებამოსილებათა შეცდომა"

#: ../src/applet.c:2137
msgid ""
"Your user does not have permissions to create additional logins.  Check your "
"<i>~/.Xauthority</i> setup."
msgstr ""
"თქვენს მომხმარებელს არა აქვს დამატებითი რეგისტრაციის უფლება. შეამოწმეთ "
"თქვენი <i>~/.Xauthority</i> ფაილი."

#: ../src/applet.c:2291
msgid "Missing Required File"
msgstr "აუცილებელი ფაილი  არ არის ნაპოვნი"

#: ../src/applet.c:2293
#, c-format
msgid ""
"The User Selector's interfaces file, `%s', could not be opened. It is likely "
"that this application was not properly installed or configured."
msgstr ""
"მომხმარებელთა სელექტორის ინტერფეისის ფაილი `%s' ვერ გაიხსნა. როგორც სჩანს, "
"ეს პროგრამა არასწორადაა ინსტალირებული ან კონფიგურირებული."

#: ../src/applet.c:2369
#, c-format
msgid "Can't lock screen: %s"
msgstr "შეუძლებელია ეკრანის დაბლოკვა: %s"

#: ../src/applet.c:2383
#, c-format
msgid "Can't temporarily set screensaver to blank screen: %s"
msgstr "ცარიელ ეკრანზე ეკრანმცველის დროებით დაყენება შეუძლებელია: %s"

#: ../src/fusa-display.c:119 ../src/fusa-user.c:150
msgid "Manager"
msgstr "მმართველი"

#: ../src/fusa-display.c:120
msgid "The manager which owns this object."
msgstr "მმართველი, რომელსაც ეკუთვნის ეს ობიექტი."

#: ../src/fusa-display.c:127
msgid "Name"
msgstr "სახელი"

#: ../src/fusa-display.c:128
msgid "The name of the X11 display this object refers to."
msgstr "X11-დისპლეი, რომელსაც მიმართავს ეს ობიექტი."

#: ../src/fusa-display.c:134 ../src/fusa-user-menu-item.c:163
msgid "User"
msgstr "მომხმარებელი"

#: ../src/fusa-display.c:135
msgid "The user currently logged in on this virtual terminal."
msgstr "ამ ვირტუალურ ტერმინალზე შესული მომხმარებელი."

#: ../src/fusa-display.c:141
msgid "Console"
msgstr "კონსოლი"

#: ../src/fusa-display.c:142
msgid "The number of the virtual console this display can be found on, or %-1."
msgstr ""
"ვირტუალური კონსოლების რაოდენობა, რომელიც შეიძლება დისპლეიმ იპოვოს, ან %-1."

#  ID:210,
#: ../src/fusa-display.c:148
msgid "Nested"
msgstr "ჩადგმული"

#: ../src/fusa-display.c:149
msgid "Whether or not this display is a windowed (Xnest) display."
msgstr "აქვს თუ არა ამ დისპლეის წყობილი (Xnest) ორგანიზაცია."

#: ../src/fusa-manager.c:1022
msgid "The display manager could not be contacted for unknown reasons."
msgstr "დისპლეის მმართველი ვერ გადის კავშირზე გაურკვეველი მიზეზის გამო."

#: ../src/fusa-manager.c:1029
msgid "The display manager is not running or too old."
msgstr "დისპლეის მენეჯერი არ არის გაშვებული ან მოძველებულია."

#: ../src/fusa-manager.c:1032
msgid "The configured limit of flexible servers has been reached."
msgstr "დამყოლი სერვერების მითითებული ლიმიტი ამოწურულია"

#: ../src/fusa-manager.c:1035
msgid "There was an unknown error starting X."
msgstr "უცნობი შეცდომა X-ის გაშვებისას."

#: ../src/fusa-manager.c:1038
msgid "The X server failed to finish starting."
msgstr "X-სერვერმა ვერ მოახერხა გაშვება."

#: ../src/fusa-manager.c:1041
msgid "There are too many X sessions running."
msgstr "გაშვებულია ძალიან ბევრი X-სესია."

#: ../src/fusa-manager.c:1044
msgid "The nested X server (Xnest) cannot connect to your current X server."
msgstr "წყობილი X-სერვერი (Xnest) ვერ უერთდება თქვენს მიმდინარე X-სერვერს."

#: ../src/fusa-manager.c:1047
msgid "The X server in the GDM configuration could not be found."
msgstr "GDM-კონფიგურაციაში X-სერვერი ვერ მოინახა."

#: ../src/fusa-manager.c:1050
msgid ""
"Trying to set an unknown logout action, or trying to set a logout action "
"which is not available."
msgstr "სესიის დახურვის მცდელობა, რომელიც მიუღებელია, ან დახურვა აკრძალულია."

#: ../src/fusa-manager.c:1053
msgid "Virtual terminals not supported."
msgstr "არ არის ვირტუალური ტერმინალების მხარდაჭერა."

#: ../src/fusa-manager.c:1056
msgid "Invalid virtual terminal number."
msgstr "ვირტუალური ტერმინალის არასწორი ნომერი."

#: ../src/fusa-manager.c:1059
msgid "Trying to update an unsupported configuration key."
msgstr "დაუშვებელი გასაღების განახლების მცდელობა."

#: ../src/fusa-manager.c:1062
msgid "~/.Xauthority file badly configured or missing."
msgstr "~/.Xauthority ფაილი არასწორადაა კონფიგურირებული ან არ არსებობს."

#: ../src/fusa-manager.c:1065
msgid "Too many messages were sent to the display manager, and it hung up."
msgstr "დისპლეის მმართველი გადატვირთულია შეტყობინებებით."

#: ../src/fusa-manager.c:1069
msgid "The display manager sent an unknown error message."
msgstr "დისპლეის მმართველმა აგზავნის უცნობ შეცდომას."

#: ../src/fusa-user-menu-item.c:164
msgid "The user this menu item represents."
msgstr "მომხმარებელი, რომელიც წარმოადგენს მენიუს ამ პუნქტს."

#: ../src/fusa-user-menu-item.c:171
msgid "Icon Size"
msgstr "ხატულას ზომა"

#: ../src/fusa-user-menu-item.c:172
msgid "The size of the icon to use."
msgstr "ხატულის გამოსაყენებელი ზომა"

#: ../src/fusa-user-menu-item.c:178
msgid "Indicator Size"
msgstr "მაჩვენებლის ზომა"

#: ../src/fusa-user-menu-item.c:179
msgid "Size of check indicator"
msgstr "ინდიკატორის ზომა"

#: ../src/fusa-user-menu-item.c:184
msgid "Indicator Spacing"
msgstr "მაჩვენებლის შორისი"

#: ../src/fusa-user-menu-item.c:185
msgid "Space between the username and the indicator"
msgstr "დაშორება მომხმარებლის სახელსა და ინდიკატორს შორის"

#: ../src/fusa-user.c:151
msgid "The user manager object this user is controlled by."
msgstr "მომხმარებელთა მმართველის ობიექტი, რომელიც აკონტროლებს ამ მომხმარებელს"

#: ../src/fusa-utils.c:80
msgid "Show Details"
msgstr "დეტალების ჩვენება"

#~ msgid "GDM (The GNOME Display Manager) is not running."
#~ msgstr "GDM (გნომის დისპლეის მმართველი) არ არის გაშვებული."

#~ msgid ""
#~ "You might in fact be using a different display manager, such as KDM (KDE "
#~ "Display Manager) or xdm."
#~ msgstr ""
#~ "თქვენ შეგიძლიათ გამოიყენოთ სხვა მმართველი ,მაგალითად KDM (KDE-ს დისპლეის "
#~ "მმართველი) ან xdm."

#~ msgid ""
#~ "If you still wish to use this feature, either start GDM yourself or ask "
#~ "your system administrator to start GDM."
#~ msgstr ""
#~ "თუ თქვენ გინდათ ამ ფუნქციის გამოყენება, გაუშვით ან მიმართეთ სისტემის "
#~ "ადმინისტრატორს GDM-ის გაშვებისთვის."

#~ msgid "Cannot communicate with GDM (The GNOME Display Manager)"
#~ msgstr "დისპლეის მმართველთან (gdm-თან) დაკავშირება არ ხერხდება"

#~ msgid "Perhaps you have an old version of GDM running."
#~ msgstr "შეიძლება გაშვებულია GDM-ს ძველი ვერსია."

#~ msgid ""
#~ "Cannot communicate with gdm, perhaps you have an old version running."
#~ msgstr ""
#~ "არ ხერხდება gdm-თან დაკავშირება, შეიძლება გაშვებულია პროგრამის ძველი "
#~ "ვერსია."

#~ msgid "The allowed limit of flexible X servers reached."
#~ msgstr "ამოწურულია დამყოლი X-სერვერების ლიმიტი."

#~ msgid "There were errors trying to start the X server."
#~ msgstr "შეცდომა X-სერვერის გაშვების მცდელობისას."

#~ msgid "The X server failed.  Perhaps it is not configured well."
#~ msgstr "X-სერვერის შეცდომა.  შეიძლება არ არის სწორად კონფიგურირებული."

#~ msgid "Too many X sessions running."
#~ msgstr "გაშვებულია ძალიან ბევრი X-სესია."

#~ msgid ""
#~ "The nested X server (Xnest) cannot connect to your current X server.  You "
#~ "may be missing an X authorization file."
#~ msgstr ""
#~ "წყობილი X-სერვერი (Xnest) ვერ უკავშირდება თქვენს X-სერვერს. შეიძლება "
#~ "დაკარგული იყოს X-უფლებამოსილებათა ფაილი."

#~ msgid ""
#~ "The nested X server (Xnest) is not available, or gdm is badly "
#~ "configured.\n"
#~ "Please install the Xnest package in order to use the nested login."
#~ msgstr ""
#~ "წყობილი X-სერვერი (Xnest) მიუწვდომელია, ან gdm არასწორადაა "
#~ "კონფიგურირებული.\n"
#~ "დააყენეთ Xnest პაკეტი წყობაში რეგისტრაციისათვის."

#~ msgid ""
#~ "The X server is not available, it is likely that gdm is badly configured."
#~ msgstr "X-სერვერი მიუწვდომელია, ალბათ gdm არასწორადაა კონფიგურირებული."

#~ msgid "Trying to change to an invalid virtual terminal number."
#~ msgstr "ტერმინალის ნომრის არასწორით შეცვლის მცდელობა."

#~ msgid ""
#~ "You do not seem to have authentication needed be for this operation.  "
#~ "Perhaps your .Xauthority file is not set up correctly."
#~ msgstr ""
#~ "თქვენ, როგორც სჩანს, არა გაქვთ ამ ოპერაციის ჩატარების უფლებამოსილება, ან "
#~ "თქვენი X-უფლებამოსილებები არასწორადაა დაყენებული."

#~ msgid "Too many messages were sent to gdm and it hung upon us."
#~ msgstr "gdm გადატვირთულია შეტყობინებებით."

#~ msgid "Unknown error occurred."
#~ msgstr "დაიშვა უცნობი შეცდომა."
